# README #

Ta aplikacja jest prostym projektem będącym pośrednikiem w dostępie do API serwisu GitHub. Projekt przeredagowany w ramach prezentacji przykładowego fragmentu kodu.

### Treść zadania ###

Treść zadania znajduje się w pliku `resources/GitHub API - Zadanie.pdf`.

### Instrukcja uruchomienia ###

Zależności projektu są zarządzane przez Composera. Pobrać można go ze [strony projektu](https://getcomposer.org/). Następnie należy pobrać niezbędne dane poleceniem:

	php composer.phar install

Po zakończeniu pobierania należy:

* Skierować parametr Document Root konfiguracji Virtual Host aplikacji wskazując katalog `public/`,
* Zapewnić, aby katalog `cache/` oraz `logs/` były zapisywalne.

Aby uruchomić aplikację w środowisku deweloperskim można również skorzystać z poniższej komendy:

    php composer.phar start

Wykorzysta ona wbudowany w PHP serwer webowy do wystawienia aplikacji na pod adresem `localhost:8080`. Aby uruchomić zestaw testów napisanych w `PHPUnit` można uruchomić następującą komendę:

    php composer.phar test

Dodatkowych informacji można szukać w komentarzach umieszczanych w konkretnych plikach.

### Dodatkowe informacje ###

Kluczowe pliki i katalogi w projekcie:

* `cache/` - katalog plików tymczasowych używanych przez klasę Cache, musi być zapisywalny,
* `logs/` - folder logów, musi być zapisywalny,
* `public/` - katalog, na który powinien wskazywać VirtualHost, zawiera plik `index.php` uruchamiający aplikację,
* `src/` - folder z logiką aplikacji podzieloną na pliki: klasy, zależności, ścieżki oraz tablicę ustawień,
* `templates/` - katalog szablonów,
* `tests/` - folder z testami funkcjonalnymi API.

W pliku `src/classes.php` znajduje się kod klas aplikacji, których funkcja jest następująca:

* `Connect` - zawiera metodę do łączenia się z innymi adresami i pobieraniu danych z wykorzystaniem `Curl`,
* `GitHubConnect` - klasa dziedzicząca po `Connect`, udostępnia metodę przygotowaną specjalnie do łączenia z API GitHuba i zwracania danych w formacie założonym w treści zadania,
* `Cache` - udostępnia mechanizm do cache'owania wyników, w ramach możliwości z wykorzystaniem Memcache lub pamięci plikowej.

Korzystanie z aplikacji może wymagać modyfikacji pliku `src/settings.php` zawierającego konfigurację aplikacji, a który dla łatwiejszego uruchomienia jest dodany do systemu kontroli wersji. Klucze, których wartości można zmieniać:

* `settings.displayErrorDetails` - aplikacja może działać w trybie produkcyjnym lub dev, włączenia tego pierwszego można dokonać przez ustawienie zmiennej serwerowej `$_ENV['production']`,
* `settings.cache.memcache.host/port` - adres hosta i numer portu wykorzystywany do połączenia z instancją Memcache, w przypadku braku lub nieprawidłowych danych aplikacja korzystać będzie z cache plikowego,
* `settings.github.username/token` - nazwa użytkownika i wygenerowany w panelu GitHuba token do połączeń z API serwisu.

Pozostałe fragmenty kodu zostały opisane komentarzami w odpowiednich plikach aplikacji.