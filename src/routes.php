<?php

/**
 * Lista wszystkich ścieżek w aplikacji
 *
 * @author     Damian Sobczak <damian@sobczak.ovh>
 * @package    GitHub API
 */

use Slim\Http\Request;
use Slim\Http\Response;

// Ścieżka domyślna z komunikatem
$app->get('/', function (Request $request, Response $response, array $args) {
    // Log dostępu do API
    $this->logger->info("GET '/' SUCCESS");

    // Wyrenderowanie strony z komunikatem
    return $this->renderer->render($response, 'index.phtml', $args);
});

// Pobieranie informacji o repozytoriach
$app->get('/repositories/{owner}/{repository}', function (Request $request, Response $response, array $args) {
    // Argumenty żądania
    $owner = $args['owner'];
    $repository = $args['repository'];

    try {
        // Utworzenie odpowiedzi w formacie JSON
        $responseNew = $response->withJson(
            $this->github->getDetails($owner, $repository),
            200
        );

        // Log dostępu do API
        $this->logger->info("GET '/repositories/$owner/$repository' SUCCESS");

        // Zwrócenie danych
        return $responseNew;
    } catch (Exception $e) {
        // Log dostępu do API
        $this->logger->error("GET '/repositories/$owner/$repository' FAILED: " . $e->getMessage());

        // Zwrócenie użytkownikowi błędu
        return $response->withJson([
            'error' => $e->getMessage(),
        ], strpos($e->getMessage(), ': ') ? intval(substr($e->getMessage(), strpos($e->getMessage(), ': ') + 2, 3)) : 404);
    }
});
