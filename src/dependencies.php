<?php

/**
 * Lista wszystkich zależności w aplikacji
 *
 * @author     Damian Sobczak <damian@sobczak.ovh>
 * @package    GitHub API
 */

// Pobranie kontenera
$container = $app->getContainer();

// Ustawienie renderera szablonów
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];

    return new Slim\Views\PhpRenderer($settings['templatePath']);
};

// Inicjalizacja Monologa dla logów aplikacji
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];

    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));

    return $logger;
};

// Inicjalizacja serwisu pamięci podręcznej
$container['cache'] = function ($c) {
    $settings = $c->get('settings')['cache'];

    return new GitHubCache($settings);
};

// Inicjalizacja serwisu dostępu do API GitHuba
$container['github'] = function ($c) {
    $settings = $c->get('settings');

    return new GitHubConnect($settings, $c->get('cache'));
};
