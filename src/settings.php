<?php

/**
 * Tablica w konfiguracją aplikacji
 *
 * @author     Damian Sobczak <damian@sobczak.ovh>
 * @package    GitHub API
 */

return [
    'settings' => [
        // Ustawienia ogólne
        'displayErrorDetails'    => ART_ENVIRONMENT == 'dev' ? true : false, // wyświetlanie błędów, na produkcji niezbędne "false"
        'addContentLengthHeader' => false,                                   // zezwala serwerowi www wysłać nagłówek Content-Length

        // Ustawienia renderera
        'renderer' => [
            'templatePath' => ART_ROOT_DIR . '/templates/', // ścieżka do katalogu szablonów
        ],

        // Ustawienia klasy połączenia przez Curl
        'connect' => [
            'connectionMethod' => 'curl',                                        // metoda połączenia z API
            'curl'             => [
                'userAgent'      => 'slim-app.allegro-recruitment-task.connect',
                'withHeader'     => true,
                'returnTransfer' => true,
                'timeOut'        => 15,
                'followLocation' => true,
                'sslVerifyHost'  => 0,
                'sslVerifyPeer'  => 0,
            ],
        ],

        // Ustawienia cache
        'cache' => [
            'filePath' => ART_ROOT_DIR . '/cache/', // ścieżka do katalogu cache
            'lifetime' => 300,                      // czas ważności cache
            'memcache' => [
                'host' => '127.0.0.1',              // adres serwera Memcache
                'port' => '11211',                  // port serwera Memcache
            ],
        ],

        // Ustawienia Monologa
        'logger' => [
            'name'  => 'slim-app',                                                               // nazwa aplikacji
            'path'  => isset($_ENV['docker']) ? 'php://stdout' : ART_ROOT_DIR . '/logs/app.log', // wyjście dla loga
            'level' => \Monolog\Logger::DEBUG,                                                   // poziom działania
        ],

        // Ustawienia połączenia z API GitHuba
        'github' => [
            'address'  => 'https://api.github.com',                   // adres API
            'username' => 'TheSpeed',                                 // nazwa użytkownika
            'token'    => 'e6e0e4a8e613244848772a84982cd03a13628e13', // osobisty token dostępu do API
            'paths'    => [
                'repository' => '/repos/',                            // ścieżka do danych repozytorium
            ],
        ],
    ],
];
