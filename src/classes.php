<?php

/**
 * Wszystkie dodatkowe klasy w aplikacji
 *
 * @author     Damian Sobczak <damian@sobczak.ovh>
 * @package    GitHub API
 */

/**
 * Trait ConnectByCurl
 *
 * Wydzielona metoda do nawiązywania połączeń do API z wykorzystaniem curl
 *
 * @package GitHub API
 */
trait ConnectByCurl
{
    /**
     * Pobranie danych i zwrócenie odpowiedzi według żądania
     *
     * @param string     $requestUrl
     * @param string     $username
     * @param string     $requestType
     * @param array      $requestData
     * @param array|null $curlSettings
     *
     * @throws Exception
     *
     * @return array
     */
    public function performCurlRequest($requestUrl, $username = '', $requestType = 'GET', $requestData = array(), $curlSettings = null)
    {
        // Utworzenie handlera curl
        $curlHandler = curl_init();

        // Ustawienie adresu połączenia
        curl_setopt($curlHandler, CURLOPT_URL, $requestUrl);

        // Ustawienie parametrów połączenia
        if (is_array($curlSettings)) {
            curl_setopt($curlHandler, CURLOPT_TIMEOUT,        $curlSettings['timeOut']);
            curl_setopt($curlHandler, CURLOPT_USERAGENT,      $curlSettings['userAgent']);
            curl_setopt($curlHandler, CURLOPT_HEADER,         $curlSettings['withHeader']);
            curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, $curlSettings['returnTransfer']);
            curl_setopt($curlHandler, CURLOPT_FOLLOWLOCATION, $curlSettings['followLocation']);
            curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, $curlSettings['sslVerifyHost']);
            curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, $curlSettings['sslVerifyPeer']);
        }

        // Dodanie danych logowania do API
        if ($username) {
            curl_setopt($curlHandler, CURLOPT_USERPWD, $username);
        }

        // Wybranie typu żądania
        switch ($requestType) {
            case 'GET':
                curl_setopt($curlHandler, CURLOPT_HTTPGET, true);
                break;

            case 'POST':
                curl_setopt($curlHandler, CURLOPT_POST, true);
                curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $requestData);
                break;

            case 'PUT':
                curl_setopt($curlHandler, CURLOPT_PUT, true);
                curl_setopt($curlHandler, CURLOPT_INFILE, $file = fopen(ART_ROOT_DIR . $requestData['file'], 'r'));
                curl_setopt($curlHandler, CURLOPT_INFILESIZE, strlen($requestData['size']));
                break;

            default:
                throw new Exception('Invalid request type!');
        }

        // Wykonanie żądania do API
        $response     = curl_exec($curlHandler);
        $status       = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
        $headerLength = curl_getinfo($curlHandler, CURLINFO_HEADER_SIZE);

        // Zamknięcie handlera
        curl_close($curlHandler);

        // Oddzielenie nagłówka od treści
        if (is_array($curlSettings) && $curlSettings['withHeader']) {
            $header = substr($response, 0, $headerLength);
            $body   = substr($response, $headerLength);
        } else {
            $header = null;
            $body = $response;
        }

        // Zwrócenie wyniku połączenia
        return [
            'status' => $status,
            'header' => $header,
            'body'   => $body,
        ];
    }
}

/**
 * Klasa Connect
 *
 * Służy do nawiązywania połączeń do API z wykorzystaniem curl
 *
 * @package GitHub API
 */
class Connect
{
    // Dodanie możliwych metod połączenia
    use ConnectByCurl;

    /**
     * Tablica z ustawieniami połączenia
     *
     * @var array $settings
     */
    private $connectionParameters = null;

    /**
     * Konstruktor klasy Connect
     *
     * @var array $parameters
     */
    public function __construct($parameters)
    {
        // Zapisanie parametrów połączenia
        $this->connectionParameters = $parameters;
    }

    /**
     * Pobranie danych ze wskazanego adresu
     *
     * @param string  $requestUrl
     * @param string  $username
     * @param string  $requestType
     * @param array   $requestData
     *
     * @throws Exception
     *
     * @return mixed
     */
    public function getData($requestUrl, $username = '', $requestType = 'GET', $requestData = array())
    {
        // Stworzenie właściwej nazwy funkcji
        $method = 'perform' . ucfirst($this->connectionParameters['connectionMethod']) . 'Request';

        // Użycie odpowiedniej metody do połączenia
        $response = $this->$method(
            $requestUrl,
            $username,
            $requestType,
            $requestData,
            $this->connectionParameters[$this->connectionParameters['connectionMethod']]
        );

        // Zwrócenie odpowiedzi w zależności od statusu HTTP
        if ($response['status'] === 200) {
            return $response['body'];
        }

        // Błąd połączenia
        throw new Exception('Invalid response status: ' . $response['status'] . '!');
    }
}

/**
 * Klasa GitHubConnect
 *
 * Służy do połączenia z API serwisu GitHub
 *
 * @package GitHub API
 */
class GitHubConnect extends Connect
{
    /**
     * Obiekt dostępu do pamięci podręcznej
     *
     * @var Cache $settings
     */
    private $cache = null;

    /**
     * Tablica z ustawieniami połączenia do API
     *
     * @var array $settings
     */
    private $githubParameters = null;

    /**
     * Konstruktor klasy GitHub
     *
     * @var array $parameters
     * @var Cache $cache
     */
    public function __construct($parameters, $cache = null)
    {
        // Wywołanie konstuktora rodzica
        parent::__construct($parameters['connect']);

        // Zapisanie parametrów połączenia i obiektu Cache
        $this->githubParameters = $parameters['github'];
        $this->cache            = $cache;
    }

    /**
     * Pobranie szczegółów wskazanego repozytorium
     *
     * @param string $owner
     * @param string $repository
     *
     * @throws Exception
     *
     * @return array
     */
    public function getDetails($owner, $repository)
    {
        // Sprawdzenie, czy rekord istnieje w cache
        if ($data = $this->cache->get("$owner/$repository")) {
            return $data;
        } else {
            // Wykonanie żądania do API serwisu GitHub
            $details = $this->getData(
                $this->githubParameters['address'] . $this->githubParameters['paths']['repository'] . "$owner/$repository",
                $this->githubParameters['username'] . ':' . $this->githubParameters['token']
            );

            // Sprawdzenie, czy żądanie się powiodło
            if ($details !== false) {
                // Zdekodowanie ciągu
                $details = json_decode(
                    $details,
                    true,
                    512,
                    JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
                );

                // Wybranie konkretnych wartości
                $data = [
                    'fullName'    => $details['full_name'],
                    'description' => $details['description'],
                    'cloneUrl'    => $details['clone_url'],
                    'stars'       => $details['stargazers_count'],
                    'createdAt'   => $details['created_at'],
                ];

                // Zapisanie danych w cache
                $this->cache->set("$owner/$repository", $data);

                // Zwrócenie wyniku
                return $data;
            }
        }

        // Nie udało się połączyć
        throw new Exception('API connection failed!');
    }
}

/**
 * Klasa Cache
 *
 * Służy do przechowywania w odpowiedniej pamięci danych tymczasowych
 *
 * @package GitHub API
 */
class Cache
{
    /**
     * Tablica z danymi konfiguracyjnymi cache
     *
     * @var array $settings
     */
    private $cacheParameters = null;

    /**
     * Dostęp do systemu cache w Memcached
     *
     * @var Memcache $memcachedStorage
     */
    private $memcachedStorage = null;

    /**
     * Dostęp do systemu cache plikowego
     *
     * @var string $fileStorage
     */
    private $fileStorage = null;

    /**
     * Konstruktor klasy Cache
     *
     * @var array $parameters
     */
    public function __construct($parameters)
    {
        // Zapisanie parametrów systemu cache
        $this->cacheParameters = $parameters;

        // Sprawdzenie czy Memcached jest dostępny
        if (extension_loaded('memcache') && class_exists('Memcache')) {
            $memcache = new Memcache;

            // Połączenie z serwerem Memcache
            if ($memcache->connect($this->cacheParameters['memcache']['host'], $this->cacheParameters['memcache']['port'])) {
                $this->memcachedStorage = $memcache;
            }
        }

        // Sprawdzenie czy cache plikowy jest dostępny
        if (is_dir($this->cacheParameters['filePath']) && is_writable($this->cacheParameters['filePath'])) {
            $this->fileStorage = $this->cacheParameters['filePath'];
        }
    }

    /**
     * Odczytanie danych z cache
     *
     * @param string $key
     *
     * @return boolean|mixed
     */
    public function get($key)
    {
        // Próba odczytu z cache Memcache
        if ($this->memcachedStorage && $data = $this->memcachedStorage->get($key)) {
            // Zwrócenie znalezionych danych
            return $data;
        }

        // Próba odczytu z cache plikowego
        if ($this->fileStorage && file_exists($this->fileStorage . $key)) {
            // Sprawdzenie, czy dane są aktualne
            $acceptedTime = time() - $this->cacheParameters['lifetime'];

            if (filemtime($this->fileStorage . $key) >= $acceptedTime) {
                // Zwrócenie danych odczytanych z pliku
                return file_get_contents($this->fileStorage . $key);
            } else {
                // Usunięcie przestarzałego pliku
                unlink($this->fileStorage . $key);
            }
        }

        // Nie znaleziono w cache
        return false;
    }

    /**
     * Zapisanie danych do cache
     *
     * @param string $key
     * @param mixed $data
     *
     * @return boolean
     */
    public function set($key, $data)
    {
        // Próba zapisu w cache Memcache
        if ($this->memcachedStorage) {
            // Zapisanie danych w Memcache
            if ($this->memcachedStorage->set($key, $data, false, $this->cacheParameters['lifetime'])) {
                return true;
            }
        }

        // Próba zapisu w cache plikowym
        if ($this->fileStorage) {
            // Zapisanie danych w pliku
            if (file_put_contents($this->fileStorage . $key, $data, LOCK_EX)) {
                return true;
            }
        }

        // Nie udało się zapisać w żadnym systemie cache
        return false;
    }

    /**
     * Wyczyszczenie danych z cache
     *
     * @param string $key
     *
     * @return boolean
     */
    public function clear($key)
    {
        // Czyszczenie danych z Memcached
        if ($this->memcachedStorage) {
            // Wyrzucenie klucza z serwera
            $this->memcachedStorage->delete($key);
        }

        // Czyszczenie danych z cache plikowego
        if ($this->fileStorage && file_exists($this->fileStorage . $key)) {
            // Wyrzucenie pliku z cache
            unlink($this->fileStorage . $key);
        }

        // Usuwanie powiodło się
        return true;
    }
}

/**
 * Klasa GitHubCache
 *
 * Przystosowanie klasy Cache do obsługi w pamięci danych tymczasowych danych GitHuba
 *
 * @package GitHub API
 */
class GitHubCache extends Cache
{
    /**
     * Konstruktor klasy GitHubCache
     *
     * @var array $parameters
     */
    public function __construct($parameters)
    {
        // Uruchomienie konstruktora klasy rodzica
        parent::__construct($parameters);
    }

    /**
     * Wyszukanie i rozkodowanie danych zapisanych w cache
     *
     * @param string $key
     *
     * @return boolean|mixed
     */
    public function get($key)
    {
        // Wyliczenie klucza i czasu ważności cache
        $key = sha1($key);

        // Kontynuuj jeśli znaleziono w pamięci
        if ($data = parent::get($key)) {
            // Rozkodowanie danych
            $data = json_decode(
                $data,
                true,
                512,
                JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );

            // Zwrócenie danych
            return $data;
        }

        // Nie znaleziono w cache
        return false;
    }

    /**
     * Zakodowanie i zapisanie danych do cache
     *
     * @param string $key
     * @param mixed $data
     *
     * @return boolean
     */
    public function set($key, $data)
    {
        // Przygotowanie danych do zapisania
        $key   = sha1($key);
        $data = json_encode(
            $data,
            JSON_NUMERIC_CHECK,
            512
        );

        // Zwrócenie statusu operacji
        return parent::set($key, $data);
    }
}