<?php

namespace Tests\Functional;

class GitHubTest extends BaseTestCase
{
    /**
     * Sprawdzenie, czy strona główna wyświetla się prawidłowo
     */
    public function testGetIndex()
    {
        // Wykonanie żądania
        $response = $this->runApp('GET', '/');

        // Test poprawności odpowiedzi
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('GitHub API', (string)$response->getBody());
    }

    /**
     * Wykonanie żądania do API bez podanych parametrów
     */
    public function testGetRepositoryNoParams()
    {
        // Wykonanie żądania
        $response = $this->runApp('GET', '/repositories');

        // Test poprawności odpowiedzi
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertContains('Page Not Found', (string)$response->getBody());
        $this->assertNotContains('fullName', (string)$response->getBody());
    }

    /**
     * Wykonanie żądania do API z prawidłowymi parametrami
     */
    public function testGetRepositoryCorrect()
    {
        // Wykonanie żądania
        $response = $this->runApp('GET', '/repositories/desktop/desktop');

        // Test poprawności odpowiedzi
        $this->assertEquals(200, $response->getStatusCode());

        $details = json_decode(
            (string)$response->getBody(),
            true,
            512,
            JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        );

        $this->assertArrayHasKey('fullName', $details);
        $this->assertArrayHasKey('cloneUrl', $details);
        $this->assertArrayHasKey('createdAt', $details);
        $this->assertEquals('desktop/desktop', $details['fullName']);
        $this->assertContains('desktop.git', $details['cloneUrl']);
        $this->assertEquals('2016-05-11T15:59:00Z', $details['createdAt']);
    }

    /**
     * Wykonanie żądania do API z parametrami nieistniejącego repozytorium
     */
    public function testGetRepositoryWrong()
    {
        // Wykonanie żądania
        $response = $this->runApp('GET', '/repositories/nouser/norepository');

        // Test poprawności odpowiedzi
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertContains('error', (string)$response->getBody());
        $this->assertNotContains('fullName', (string)$response->getBody());
    }

    /**
     * Sprawdzenie, czy GitHub nie nałoży blokady dla niezalogowanego po 60+ żądaniach
     */
    public function testGetRepositoryAuthenticated()
    {
        // Pętla żądań
        for ($i = 0; $i < 75; $i++) {
            // Wykonanie żądania
            $response = $this->runApp('GET', '/repositories/nouser/norepository' . $i);

            // Test poprawności odpowiedzi
            $this->assertEquals(404, $response->getStatusCode());
        }
    }

    /**
     * Sprawdzenie, czy po pierwszym żądaniu dane są zapisywanie w cache
     */
    public function testGetRepositoryCache()
    {
        // Wykonanie żądania zapisującego cache
        $this->runApp('GET', '/repositories/desktop/desktop');

        // Wyczyszczenie cache
        $this->lastApp->getContainer()->get('cache')->clear(sha1('desktop/desktop'));

        // Test poprawności działania
        $this->assertEquals(false, $this->lastApp->getContainer()->get('cache')->get('desktop/desktop'));

        // Ponowne zapisanie cache
        $this->runApp('GET', '/repositories/desktop/desktop');

        // Test poprawności działania
        $this->assertNotEquals(false, $this->lastApp->getContainer()->get('cache')->get('desktop/desktop'));
    }
}
