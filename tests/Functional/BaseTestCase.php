<?php

/**
 * Główny plik, punkt wejściowy dla aplikacji
 *
 * @author     Damian Sobczak <damian@sobczak.ovh>
 * @package    GitHub API
 */

namespace Tests\Functional;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;

/**
 * Klasa BaseTestCase
 *
 * Tworzy podstawowe środowisko do uruchomienia testów aplikacji
 *
 * @package GitHub API
 */
class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Zmienna określająca środowisko dla testów
     *
     * @var boolean $usedEnvironment
     */
    protected $usedEnvironment = 'prod';

    /**
     * Zmienna z ostatnim obiektem aplikacji
     *
     * @var App $lastApp
     */
    protected $lastApp = null;

    /**
     * Przetwórz aplikację dla zadanej metody i adresu
     *
     * @param string            $requestMethod
     * @param string            $requestUri
     * @param array|object|null $requestData
     *
     * @return \Slim\Http\Response
     */
    public function runApp($requestMethod, $requestUri, $requestData = null)
    {
        // Ustawienie katalogu ROOT aplikacji i środowiska
        if (!defined('ART_ROOT_DIR')) {
            define('ART_ROOT_DIR', realpath(dirname(__FILE__) . '/../..'));
            define('ART_ENVIRONMENT', $this->usedEnvironment);
        }

        // Stworzenie sztucznego środowiska dla testów
        $environment = Environment::mock(
            [
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI'    => $requestUri
            ]
        );

        // Stworzenie obiektu żądania na podstawie środowiska
        $request = Request::createFromEnvironment($environment);

        // Dodanie danych żądania, jeśli istnieją
        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        // Stworzenie obiektu odpowiedzi
        $response = new Response();

        // Inicjalizacja aplikacji
        $settings = require ART_ROOT_DIR . '/src/settings.php';
        $app = new App($settings);

        // Dołączenie klas
        require_once ART_ROOT_DIR . '/src/classes.php';

        // Utworzenie zależności
        require ART_ROOT_DIR . '/src/dependencies.php';

        // Zarejestrowanie middleware
        require ART_ROOT_DIR . '/src/middleware.php';

        // Zarejestrowanie ścieżek
        require ART_ROOT_DIR . '/src/routes.php';

        // Uruchomienie testu
        $response = $app->process($request, $response);

        // Zapisanie obiektu app do dalszego działania
        $this->lastApp = $app;

        // Zwrócenie odpowiedzi
        return $response;
    }
}
