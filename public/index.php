<?php

/**
 * Główny plik, punkt wejściowy dla aplikacji
 *
 * @author     Damian Sobczak <damian@sobczak.ovh>
 * @package    GitHub API
 */

// Ustawienie katalogu ROOT aplikacji
if (!defined('ART_ROOT_DIR')) {
    define('ART_ROOT_DIR', realpath(dirname(__FILE__) . '/..'));

    // Wykrycie środowiska, w którym uruchamiana jest aplikacja
    if (isset($_ENV['production']) && $_ENV['production']) {
        define('ART_ENVIRONMENT', 'prod');
    } else {
        define('ART_ENVIRONMENT', 'dev');
    }
}

// Rzucanie statycznego pliku przy żądaniu z serwera CLI
if (PHP_SAPI == 'cli-server') {
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = ART_ROOT_DIR . '/public/' . $url['path'];

    if (is_file($file)) {
        return false;
    }
}

// Dołączenie autoloada i innych klas
require ART_ROOT_DIR . '/vendor/autoload.php';
require ART_ROOT_DIR . '/src/classes.php';

// Start sesji
session_start();

// Inicjalizacja aplikacji
$settings = require ART_ROOT_DIR . '/src/settings.php';
$app = new \Slim\App($settings);

// Utworzenie zależności
require ART_ROOT_DIR . '/src/dependencies.php';

// Zarejestrowanie middleware
require ART_ROOT_DIR . '/src/middleware.php';

// Zarejestrowanie ścieżek
require ART_ROOT_DIR . '/src/routes.php';

// Uruchomienie aplikacji
$app->run();
